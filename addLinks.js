window.addEventListener("load", (ev) => {
    let links = document.getElementById("links").children;
    for (let link of links) {
        if (link.getAttribute("lnkto")) {
            link.addEventListener("click", (ev)=>{
                let target = ev.target;
                let link;
                if (target.nodeName == "SPAN") {
                    target = target.parentElement;
                }
                link = target.getAttribute("lnkto");
                
                location.href = link;
            })
        }
        
    }
})
